users:
  yuriy:
    uid: 1001
    password: $6$DY4gNRBmcfB8.jLh$8Ac.0aB9CTUd4Wgt/YBHURFsGpjvZk26gchaxv8.4i0GRptc8vmAnPO0zn824wuTkiFVvge2rPAxBUPatb47v/
    fullname: "Yuriy Zhigul"
    sh: /bin/zsh
    groups: ["users", "sudo", "tcpdump"]
    public-ssh-keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDIcZ6H/cIkv7KO2knI8URTdJH2PKD22463sY0D0KBoAm4vO4ufQfLFybyoPyqLX9PPJ2m5OClZsJaXUqa4zhzbhD7ATnHL0HuTX2d/Wg2eenmoKwfs24KvXmZq3V5xhktbiOHBhnyr46eMwdjIG12Yids/GaBXz7HwJsVXWMpDFAmnJERvd420YrlWSz6zYxLl/scwnK1B2ijRlWWxdStGavyiAPpR4O5gXMb6yoX42kUMxJgNYW0qAfrIO5EWLhs9yaoeWNgkwPO0D9O3GnEBr+SqSkGSRVj0M8frAomfw0aFvk2PF4jd434tohV20x1SKH3KK8rtJeHSWIqV7apv7Bb6pfqwSfpa/PLUGvwvWHRrRTz84oAKhFulkeKclQnUYdIj10quzQPI5kSnABnuaWoaWX9LMmqfW5GDqREa4DGcfR+GVj03d2FDFrozY19bVkSp/EZWZCieTLmAZ2wKH3BTTvSGRNDm77ofkC7DWlZPKYMkozVFnp/avfRtWZ0= yzhigulskiy@yzhigulskiy-ThinkPad-X1-Carbon-7th
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ/pcGcY3AO4yx4YSsJdVNj3QjLLz9/48uiKn1RtEXnQ yzhigulskiy@yzhigulskiy-ThinkPad-X1-Carbon-7th  
  alex:
    uid: 1002
    password: $6$u840TTlzUEgKKhL8$JYXJOQd6p28i4nEAluffl3xiZ12LPdxCKLAV6LBZRzsFSw/RX0aTi5d9LAUpekvr1.d3shCSaFhwEmPuqTOSn0
    fullname: "Alexander Zhigulskiy"
    homepath: "/opt/alex"
    sh: /bin/sh
    public-ssh-keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ/pcGcY3AO4yx4YSsJdVNj3QjLLz9/48uiKn1RtEXnQ yzhigulskiy@yzhigulskiy-ThinkPad-X1-Carbon-7th
  mark:
    uid: 1003
    password: $6$NuCfeiY6ZRjSeB3t$R8JVslBWAGCbD3pyEmf0mQp7qZyM3JbdkVEgK99RvcxPggD9tujSg0ZhReDwb.M3YvjuTxORvVrwo6k884ct21
    fullname: "Mark Zuckerberg"
    sh: /usr/sbin/nologin
    public-ssh-keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ/pcGcY3AO4yx4YSsJdVNj3QjLLz9/48uiKn1RtEXnQ yzhigulskiy@yzhigulskiy-ThinkPad-X1-Carbon-7th
  bob:
    uid: 1004
    password: $6$7XztwfUit2ntM0W9$ESZrjroRR/4vndqT.UkBirv9u2/9v20izU2VECGqCWrn3m15GJSGjwW1YUFG464tuxXF4F.moaQjUVEMYbd2V1
    sh: /bin/bash
    public-ssh-keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ/pcGcY3AO4yx4YSsJdVNj3QjLLz9/48uiKn1RtEXnQ yzhigulskiy@yzhigulskiy-ThinkPad-X1-Carbon-7th
  alice:
    uid: 1005    
    fullname: "Alice Cooper"
    public-ssh-keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ/pcGcY3AO4yx4YSsJdVNj3QjLLz9/48uiKn1RtEXnQ yzhigulskiy@yzhigulskiy-ThinkPad-X1-Carbon-7th
      