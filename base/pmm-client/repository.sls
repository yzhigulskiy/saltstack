{%- from "base/pmm/map.jinja" import pmm with context %}

pmm-client_apt_repository:
  pkgrepo.managed:
    - humanname: PMM APT repository
    - name: {{ pmm.common.repo }}
    - file: /etc/apt/sources.list.d/pmm.list
    - refresh: True
