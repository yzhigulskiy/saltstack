{%- from "base/pmm/map.jinja" import pmm with context %}

pmm2-client:
  pkg:
    - installed

{% if salt['grains.get']('pmm-client-register') != true %}

pmm2-configure:
  cmd.run:
    - name: pmm-admin config --server-insecure-tls --server-url=https://admin:ahkoophei7leeSh@206.189.61.198:443 206.189.61.198 generic pg-client
  grains.present:
    - name: pmm-client-register
    - value: true 

{% endif %}
