# users :: Create new user based on minion grains

{%- from "base/users/map.jinja" import users_userlist with context %}

{% for user in users_userlist %}
  {% if user in grains['users'] %}
    users_{{ user }}:
      user.present:
        - name: {{ users_userlist[user]['name'] }}
        - uid: {{ users_userlist[user]['uid'] }}
        - password: {{ users_userlist[user]['password'] }}
        - empty_password: False
        - fullname: {{ users_userlist[user]['fullname'] }}
        - home: {{ users_userlist[user]['homepath'] }}
        - shell: {{ users_userlist[user]['sh'] }}
        - groups: {{ users_userlist[user]['groups'] }}
      ssh_auth.present:
        - user: {{ users_userlist[user]['name'] }}
        - enc: ssh-rsa
        - names: {{ users_userlist[user]['ssh_keys'] | default([]) }}
  {% endif %}
{% endfor %}