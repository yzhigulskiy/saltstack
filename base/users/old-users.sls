
{% for user, args in pillar.get('users', {}).items() %}

users_add_{{ user }}:

    user.present:
      - uid: {{ args['uid'] | default("") }}
    {% if args['password'] is defined %}
      - password: {{ args['password'] }}
    {% endif %}
      - shell: {{ args['sh'] | default(defaults['shell']) }}
      - fullname: {{ args['fullname'] | default(defaults['fullname']) }}
      - home: {{ args['homepath'] | default(defaults['homepath'] + user ) }}
      - groups:
        {% if args['groups'] is defined %}
          {% for group in args['groups'] %}
        - {{ group }}
          {% endfor %}
        {% endif %}
        - {{ defaults.group }}
    ssh_auth.present:
      - user: {{ user }}
      - enc: ssh-rsa
    {% if args['public-ssh-keys'] is defined %}
      - names:
        {% for key in args['public-ssh-keys'] %}
        - {{ key }}
        {% endfor %}
    {% endif %}

{% endfor %}

# ---
# Jinja
# {% set defaults = {
#   'shell': "/bin/false",
#   'fullname': "NOT DEFINED",
#   'homepath': "/home/",
#   'group': "users"
# } %}