# users :: Delete user if it is not exists in minios grains

{%- from "base/users/map.jinja" import users_userlist with context %}

{% for user in users_userlist %}
  {% if user not in grains['users'] %}
    users_{{ user }}:
      user.absent:
        - name: {{ users_userlist[user]['name'] }}
        - purge: True
  {% endif %}
{% endfor %}